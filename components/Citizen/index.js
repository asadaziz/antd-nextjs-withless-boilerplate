import styles from "./style.module.css";

export default()=>(
<div className={styles.citizenContainer}>
<div>
    <p className={styles.citizenMainText}>Apps For Citizens</p>
    <p className={styles.citizenSecondryText}>digital nomads life management apps.</p>
</div>
<div className="row">
    <div className="col-sm appsContainer text-center">
        <img alt="apps" src="/apps.png" className="img-fluid apps"/>
    </div>
    <div className="col-sm boxContainer">
        <div className="row">
            <div className="col-sm">
                <div className="row">
                <div className="box">
                    <img src={message} alt="message" className="img-fluid boxImages"/>
                </div>
                    <p className="textofBoxes">Community chat messenger</p>
                </div>
            </div> 
            <div className="col-sm">
                    <div className="row">
                    <div className="box">
                    <img src={flight} alt="flight" className="img-fluid boxImages"/>
                    </div>
                    <p className="textofBoxes">dN flight search</p>
                    </div>
            </div>
        </div>
        <div className="row">
            <div className="col-sm">
                <div className="row">
                <div className="box">
                    <img src={suite} alt="message" className="img-fluid boxImages"/>
                </div>
                <p className="textofBoxes">trips</p>
                </div>
            </div> 
            <div className="col-sm">
                    <div className="row">
                    <div className="box">
                    <img src={heart} alt="flight" className="img-fluid boxImages"/>
                    </div>
                    <p className="textofBoxes">Digital Nomads dating</p>
                    </div>
            </div>
        </div>
        <div className="row">
            <div className="col-sm">
                <div className="row">
                <div className="box">
                    <img src={travel} alt="message" className="img-fluid boxImages"/>
                </div>
                <p className="textofBoxes">travel together matching</p>
                </div>
            </div> 
            <div className="col-sm">
                    
            </div>
        </div>
    </div>
</div>
</div>
)